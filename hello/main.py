#!/usr/bin/env python
#coding=utf-8

from flask import Flask, render_template, url_for
app = Flask(__name__)

@app.route('/')
def hello_world():
    return 'hello world'

@app.route('/test/<user>')
def test(user):
    return 'hello, %s' % user

@app.route('/index')
def index():
	return render_template('hello.html') + url_for('static', filename="style.css")

if __name__ == "__main__":
    app.run(debug=True)