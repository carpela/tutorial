#!/usr/bin/env python
#encoding: utf-8

class Context(object):
	def __init__(self):
		print "__init__"

	def __exit__(self, exc_type, exc_val, exc_tb):
		print "__exit__"

	def __enter__(self):
		print "__enter__"

with Context():
	print "hehe"