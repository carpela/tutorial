#!/usr/bin/env python
#encoding: utf-8

from multiprocessing import Pipe, Process

def f(con):
	con.send(['pu',43,'heh'])
	con.close()

if __name__ == "__main__":
	parent_con, child_con = Pipe()
	p = Process(target=f, args=(child_con,))
	p.start()
	print parent_con.recv()
	p.join()