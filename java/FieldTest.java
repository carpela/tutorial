import java.lang.reflect.*;
import java.util.*;

class Person
{
    public int age = 10;
    private String name;
    public void setAge(int age)
    {
        this.age = age;
    }

    public void setName(String name)
    {
        this.name = name;
    }
}

public class FieldTest
{
    public static void main(String[] args) throws Exception
    {
        Person p = new Person();
        Class c = p.getClass();
        Field[] tmp = c.getDeclaredFields();
        AccessibleObject.setAccessible(tmp, true);
        for(Field f: tmp)
        {
            System.out.println(f.get(p));
        }

       Field f = c.getDeclaredField("age");
        f.setAccessible(true);
        p.setAge(30);
        System.out.println(f.get(p));
        Person p2 = new Person();
        System.out.println(f.get(p2));
    }
}
