#!/usr/bin/env python
#encoding: utf-8

from contextlib import contextmanager
from contextlib import closing

@contextmanager
def make_context():
	print 'enter'
	try :
		yield {}
	except RuntimeError, err :
		print 'error' , err
	finally :
		print 'exit'

with make_context() as value :
	print value

class Door(object):
	def open(self):
		print "open"
	def close(self):
		print "close"

with closing(Door()) as f:
	f.open()