#!/usr/bin/env python
#encoding: utf-8

class WithinContext(object):
	def __init__(self, context):
		print "__init__(WithinContext) from %s" % context
	def do(self):
		print "WithinContext doing something"
	def __del__(self):
		print "__del__(WithinContext)"

class Context(object):
	def __init__(self):
		print "__init__(Context)"

	def __exit__(self, exc_type, exc_val, exc_tb):
		print "__exit__(Context)"

	def __enter__(self):
		print "__enter__(Context)"
		return WithinContext(self)

with Context() as a:
	a.do()
