#!/usr/bin/env python
#encoding: utf-8

import threading, time

def worker():
	print "worker"
	time.sleep(1)

for i in xrange(5):
		t = threading.Thread(target=worker)
		if i == 2:
			t.setDaemon(True)
		t.start()

if __name__ == "__main__":
	print "active threading number: %d" % threading.activeCount()
	print threading.enumerate()