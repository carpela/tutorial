#!/usr/bin/env python
#encoding: utf-8

from multiprocessing import Process, Queue

def f(q):
	q.put(['heh','hah','heihei'])

if __name__ == "__main__":
	q = Queue()
	p = Process(target=f, args=(q,))
	p.start()
	p.join()
	print q.get()